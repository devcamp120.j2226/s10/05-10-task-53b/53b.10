import models.Author;
import models.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("A", "a@gmail.com", 'm');

        Author author2 = new Author("B", "b@gmail.com", 'f');

        System.out.println("Author 1:");
        System.out.println(author1.toString());
        System.out.println("Author 2:");
        System.out.println(author2.toString());

        Book book1 = new Book("C", author1, 100);

        Book book2 = new Book("D", author2, 1000, 10);

        System.out.println("Book 1:");
        System.out.println(book1.toString());
        System.out.println("Book 2:");
        System.out.println(book2.toString());
    }
}
